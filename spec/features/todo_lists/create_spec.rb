require 'rails_helper'

describe 'When a user creates a To-Do List' do
  def create_todo_list(options={})
    options[:title] ||= 'My todo list'
    options[:description] ||= 'This is my todo list'

    visit '/todo_lists'
    click_link 'New Todo list'
    expect(page).to have_content('New Todo List')

    fill_in 'Title', with: options[:title]
    fill_in 'Description', with: options[:description]
    click_button 'Create Todo list'
  end

  it 'redirects to the todo list index page on success' do

    create_todo_list

    expect(page).to have_content('My todo list')
  end

  it 'displays an error when the todo list has no title OR title is less_than 3 chars' do
    expect(TodoList.count).to eq(0)

    create_todo_list title: ''

    expect(page).to have_content('error')
    expect(TodoList.count).to eq(0)

    visit '/todo_lists'
    expect(page).to_not have_content('Looks like the test runs')
  end

  it 'displays an error when the todo list has no description OR description is less_than 8 chars' do
    expect(TodoList.count).to eq(0)

    create_todo_list title: 'Grocery list', description: 'Food'

    expect(page).to have_content('error')
    expect(TodoList.count).to eq(0)

    visit '/todo_lists'
    expect(page).to_not have_content('Looks like the test runs')
  end
end
