require 'rails_helper'

describe 'When a user edits an existing Todo list' do
  let!(:todo_list) { TodoList.create(title: 'Groceries', description: 'My Grocery List Description') }

  def update_todo_list(options={})
    options[:title] ||= 'My todo list'
    options[:description] ||= 'This is my todo list'

    todo_list = options[:todo_list]

    visit '/todo_lists'
    within "#todo_list_#{todo_list.id}" do
      click_link 'Edit'
    end

    fill_in 'Title', with: options[:title]
    fill_in 'Description', with: options[:description]
    click_button 'Update Todo list'
  end

  it 'updates a todo list successfully with correct information' do
    # todo_list = TodoList.create(title: 'Groceries', description: 'My Grocery List Description')
    update_todo_list todo_list: todo_list,
                     title: 'New Title',
                     description: 'This is a new description'
    # Fails without this line
    todo_list.reload

    expect(page).to have_content('Todo list was successfully updated')
    expect(todo_list.title).to eq('New Title')
    expect(todo_list.description).to eq('This is a new description')
  end
end